/*
Student Name: Şemsi Yiğit Özgümüş
Student Number: 2012400207
Compile Status: Compiling
Program Status: Working
Notes: Processor input should take account of both master and the slaves. And number of slave 
processor side should be able to divide the grid of the maze
*/

#include "mpi.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define I_AM_MASTER 0


//Function declarations
void masterProcess(std::string input,int mazeSize,int *data);
int getInput(std::string inputFile,int *maze);
void extractResult(int *testVector,std::string outputFile,int mazeSize);
void assignProcessors(int mazeSize, 
                        int myrank ,
                        int num_procs ,
                        int* processor_start, 
                        int* processor_size);
int readsize(std::string inputFile);
int deadEndFinder(int *maze,int row,int column,int lineSize);

int main(int argc, char *argv[])
{
  //=======General variables==================================================================
    int mazeSize;
    int myrank, num_procs;
    int  processor_start;
    int  processor_size;
//=======MPI Commands and file================================================================
    MPI_Init(&argc, &argv);
    MPI_Comm_rank( MPI_COMM_WORLD, &myrank );
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    MPI_Request request;
    MPI_Status status;
    if(argc < 3){
      std::cout << "You entered insufficient amount of commandline arguments." << std::endl;
      exit(-1);
    }
    std::string inputFile = argv[1];  
    std::string outputFile = argv[2];
    bool deadEndPresent = true ;

//=======Maze Reading=========================================================================
    if(myrank == I_AM_MASTER){
      //get the size 
       mazeSize = readsize(inputFile);
     //  std::cout << mazeSize << std::endl;
      // Send the mazeSize to all processors
      for (int i = 0; i < num_procs; ++i){
        MPI_Send(&mazeSize, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
      } 
         
    }
     //Receive the mazeSize
     MPI_Recv(&mazeSize, 1, MPI_INT, 0, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
     //Assign the appropriate maze indexes
     assignProcessors(mazeSize,myrank,num_procs,&processor_start,&processor_size);
    // std::cout << myrank << " " << processor_size << " " << processor_start << std::endl;
     // Create necessary arrays
     int testvector[mazeSize * mazeSize];
     //Error message for the undivisible number of processors
     if(mazeSize % (num_procs-1) != 0){
      std::cout << "I can't solve this problem with indivisible number of processors!" << std::endl;
      exit(-1); 
     }
//=======Read The Maze----==================================================================
//Master process invokes mazeRead function. 
    if(myrank == I_AM_MASTER){
      masterProcess(inputFile,mazeSize,testvector);
          }
//=======Create Maze Copies=================================================================
//Sync the processors
  MPI_Barrier(MPI_COMM_WORLD);
  //for(int i =0 ; i < mazeSize ; i++)
  //Broadcast the Maze to the other processors for access
  MPI_Bcast(&testvector,
            mazeSize * mazeSize ,
            MPI_INT,
            0,
            MPI_COMM_WORLD);
//==============================Loop========================================================
while(deadEndPresent){
  deadEndPresent = false;
  // message Loops
  //In The first if There are Send Messages and in the Second there are receives
  //=========================Sends==========================================================
  if(myrank == 1){
    MPI_Isend(&testvector[((myrank * processor_size) -1) * mazeSize],
                          mazeSize,MPI_INT,
                          myrank+1,0,
                          MPI_COMM_WORLD,&request);
    MPI_Wait(&request,&status);
  }else if(myrank == num_procs-1){
    MPI_Isend(&testvector[(myrank-1) * processor_size * mazeSize],
                          mazeSize,MPI_INT,
                          myrank-1,0,
                          MPI_COMM_WORLD,&request);
  MPI_Wait(&request,&status);
  }else if (myrank != I_AM_MASTER){
    MPI_Isend(&testvector[(myrank-1) * processor_size * mazeSize],
                          mazeSize,MPI_INT,
                          myrank-1,0,
                          MPI_COMM_WORLD,&request);
    MPI_Wait(&request,&status);
    MPI_Isend(&testvector[((myrank * processor_size) -1) * mazeSize],
                          mazeSize,MPI_INT,
                          myrank+1,0,
                          MPI_COMM_WORLD,&request);
    MPI_Wait(&request,&status);
  }

  //===========================Receives====================================================
    if(myrank == 1){
      MPI_Irecv(&testvector[(myrank * processor_size ) * mazeSize],
                            mazeSize,MPI_INT,
                            myrank+1,0,
                            MPI_COMM_WORLD,&request);
      MPI_Wait(&request,&status);
    }else if(myrank == num_procs-1){
      MPI_Irecv(&testvector[((myrank-1) * processor_size -1) * mazeSize],
                            mazeSize,MPI_INT,
                            myrank-1,0,
                            MPI_COMM_WORLD,&request);
      MPI_Wait(&request,&status);
    }else if(myrank != I_AM_MASTER){
      MPI_Irecv(&testvector[((myrank-1) * processor_size -1) * mazeSize],
                            mazeSize,MPI_INT,
                            myrank-1,0,
                            MPI_COMM_WORLD,&request);
      MPI_Wait(&request,&status);
      MPI_Irecv(&testvector[(myrank * processor_size ) * mazeSize],
                            mazeSize,MPI_INT,
                            myrank+1,0,
                            MPI_COMM_WORLD,&request);
      MPI_Wait(&request,&status);
    }
      
    //This for loop part checks all the elements (each processor its own section) and 
    //updates the array accordingly 
    int block = mazeSize / (num_procs-1);
    for(int line = (myrank-1) * block ; line < myrank * block; line++){
      if (myrank == 0){ break;}
      for (int cell = 1; cell < mazeSize -1  ; cell++){
        if(testvector[(line * mazeSize) + cell] &&
         deadEndFinder(testvector,line,cell,mazeSize) ==3 &&
          myrank != 0){
            deadEndPresent = true ;
              testvector[(line * mazeSize) + cell] = 0;
              
        }
      }
    }
  
  //=================while iteration continuity==================================
  //Every processor should notify the master about whether it saw a dead end or not
  if(myrank != I_AM_MASTER){
    MPI_Send(&deadEndPresent,1,MPI_INT,0,0,MPI_COMM_WORLD);
  } 
  // Master receives the messages sequentially and checks them.
  if(myrank == I_AM_MASTER){
    for (int dead_index = 1; dead_index < num_procs; ++dead_index){
      MPI_Recv(&deadEndPresent,1,MPI_INT,dead_index,0,MPI_COMM_WORLD,&status);
      if(deadEndPresent == true) {break;}
    }
  }

  //The Master process broadcasts its dead end and updates other so that loop does not restart
  //Send all processors the new deadend values
  if(myrank == I_AM_MASTER){
    for(int processor =1;processor < num_procs; processor++){
      MPI_Send(&deadEndPresent,1,MPI_INT,processor,0,MPI_COMM_WORLD);
    }
  }
  //All processors receive new values
  if(myrank != I_AM_MASTER){
    MPI_Recv(&deadEndPresent,1,MPI_INT,0,0,MPI_COMM_WORLD,&status);
  }
  //Process syncronization before gathering
  MPI_Barrier(MPI_COMM_WORLD);  
}
MPI_Barrier(MPI_COMM_WORLD);
//==========================Gather the maze======================================
// MPI_Gather(&testvector,mazeSize * mazeSize,MPI_INT,0,MPI_COMM_WORLD);
for (int lines = 0; lines < mazeSize; ++lines){
  //All processors Broadcasts their parts to the system
  MPI_Bcast(&testvector[lines * mazeSize],
    mazeSize,MPI_INT,(lines/processor_size)+1,MPI_COMM_WORLD);
}
// Master writes the result to a file
if(myrank == I_AM_MASTER){
  extractResult(testvector,outputFile,mazeSize);
}
/* // Generate the output for Each processor

// for (int i = 0; i < mazeSize; ++i){
//   for (int j = (i * mazeSize) ; j < (i+1) * mazeSize; ++j){
//         std::cout << testvector[j] << " ";
//       }
//       std::cout << std::endl;
//     }
//     std::cout << std::endl;
*/
//==========================================================================================
    MPI_Finalize();
    return 0;
}
//==========================================================================================
void masterProcess(std::string input,int mazeSize,int *mazeRep){
   // std::vector<std::vector<int> > *mazeRep = new std::vector<std::vector<int> >();
 //   std::cout << "master" << std::endl;
    mazeSize = getInput(input, mazeRep);
//    std::cout << mazeSize << std::endl;
    // for (int i = 0; i < mazeSize; ++i){
    //   for (int j = (i * mazeSize) ; j < (i+1) * mazeSize; ++j)
    //   {
    //     std::cout << mazeRep[j] << " ";
    //   }

    //   std::cout << std::endl;
    // }
    // std::cout << std::endl;
}
//==========================================================================================
int getInput(std::string inputFile,int *maze){
    int mazeSize ;
    std::ifstream inputMaze(inputFile,std::ios_base::in);
    //std::vector<int> *line = new std::vector<int>();
    int number;
    //get the maze size;
    inputMaze >> number ;
    mazeSize = number;
    inputMaze.ignore();
    inputMaze.ignore();
    // = new int[mazeSize * mazeSize];
    for (int i = 0; i < mazeSize * mazeSize; ++i){
      inputMaze >> number ;
      maze[i] = number ;
    }
     return mazeSize;
}
//==========================================================================================
int readsize(std::string inputFile){
  std::ifstream inputMaze(inputFile,std::ios_base::in);
  int mazeSize;
  inputMaze >> mazeSize ;
  inputMaze.close();
  return mazeSize;
}
//==========================================================================================
void assignProcessors(int mazeSize, 
                        int myrank ,
                        int num_procs ,
                        int* processor_start, 
                        int* processor_size){
  
  *processor_start = (mazeSize / (num_procs -1) ) * (myrank -1);
  *processor_size = (mazeSize / (num_procs-1)) ;
  
 // std::cout << mazeSize << " " << myrank << " " << *processor_start << " " << *processor_size << std::endl;

}
//==========================================================================================
int deadEndFinder(int *maze,int row,int column,int lineSize){
  int result = 0 ;
    for(int ci = -1;ci <2 ;ci+=2){
      if((column +ci >= 0) && ((column + ci) <= (lineSize-1))){
        if( maze[(row * lineSize) + (column + ci)] == 0)
          result +=1 ;
        }
    }

    for(int ri = -1; ri < 2 ; ri+=2){
      if((row +ri >= 0) && ((row +ri) <= (lineSize-1))){
        if (maze[((row + ri) * lineSize) + column ] == 0 )
          result += 1 ;
        }
      }

    return result ;
}

//==========================================================================================
void extractResult(int *testVector,std::string outputFile,int mazeSize){
  std::ofstream output(outputFile); 
  for (int i = 0; i < mazeSize; ++i){
    for (int j = 0; j < mazeSize; ++j){
      output<<" "<<testVector[i * mazeSize + j] ;
    }
    output << std::endl;
  }
}